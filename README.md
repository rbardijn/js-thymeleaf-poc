Comparison on JsThymeleaf and JavaThymeleaf.
==================================

This repository exists to test out different features for the [jsThymeleaf](https://www.npmjs.com/package/thymeleaf) port from the Java template engine [Thymeleaf](https://thymeleaf.org). This is the jira ticket, for more information: https://atlassian.persgroep.net/jira/browse/INDEL-10.

## Js Thymeleaf:

### Installation

The js version of thymleaf sits inside of the js-thymeleaf/ folder.
Go inside the folder and install the packages, then run the script using following commands:

`cd js-thymeleaf; npm i && npm run thymeleaf && npm start;`

**You should have the latest version of nodejs**

Browse to: **http://localhost:3000/**

Most importantly, here you can find the pending tasks:

  * [Supported processors](https://github.com/ultraq/thymeleafjs/issues/21)
  * [Supported expressions](https://github.com/ultraq/thymeleafjs/issues/20)

## Java Thymeleaf:

### Installation

The java version of thymeleaf sits inside of the java-thymeleaf/ folder.
Go inside the folder and install the maven dependencies, then run the script using following commands:

`cd java-thymeleaf; mvn clean install && mvn spring-boot:run;`

**You should have at least the JAVA 1.8**

Browse to: **http://localhost:8080/**

## Additionally:

* You can find an **npm script** that copies over the thymeleaf component from the correct js dir to the correct java dir `npm run copy`
* The JavaProject is not autoreloaded so on every change you need to re-run `mvn spring-boot:run;`
* The javascript Project has a browsersync server, to run it `npm start`.

