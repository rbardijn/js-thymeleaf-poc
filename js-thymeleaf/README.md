Comparison and poc tests on jsThymeleaf vs Java Thymeleaf.
==================================

This test repository is to test out features on the [jsThymeleaf](https://www.npmjs.com/package/thymeleaf) port of the java template engine [Thymeleaf](https://thymeleaf.org)

https://atlassian.persgroep.net/jira/browse/INDEL-10

## Js Thymeleaf:

### Installation

The js version of thymleaf sits inside of the js-thymeleaf/ folder.
Go inside the folder and install the packages, then run the script using following commands:

`cd js-thymeleaf; npm i && npm run thymeleaf;`

**You should have the latest version of nodejs**

Most importantly, here you can find the pending tasks:

  * [Supported processors](https://github.com/ultraq/thymeleafjs/issues/21)
  * [Supported expressions](https://github.com/ultraq/thymeleafjs/issues/20)

## Java Thymeleaf:

### Installation

The java version of thymleaf sits inside of the java-thymeleaf/ folder.
<!-- Go inside the folder and install the packages, then run the script using following commands: -->

