import Thymeleaf from 'thymeleaf';
import YAML from 'yaml';
import fs from 'fs';
import chalk from 'chalk';

const yamlFileStream = fs.readFileSync(`./src/yml/context.yml`, 'utf8');
const thymeleafFile = `./src/thl/template.html`;
let templateEngine = new Thymeleaf.TemplateEngine({
  ...Thymeleaf.STANDARD_CONFIGURATION,
  templateResolver: (templateName) => {
    return fs.readFileSync(`./src/thl/${templateName}.html`);
  }
});

// Render template from file
templateEngine
  .processFile(thymeleafFile, YAML.parse(yamlFileStream))
  .then(result => {
    fs.writeFile("./dist/index.html", result, (err) => {
      if(err) {return console.log(err);}
      console.log(chalk.blue("./dist/index.html") + " was saved!");
    });
  });
