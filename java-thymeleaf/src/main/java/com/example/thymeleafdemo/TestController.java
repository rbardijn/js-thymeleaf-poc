package com.example.thymeleafdemo;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {

    @RequestMapping(value = "/")
    public ModelAndView getTestPage(final Model model) {
        model.addAttribute("username", "Awesome Developer");
        model.addAttribute("language", "Javascript <i>awesomeness</i>");
        model.addAttribute("shouldLearn", false);
        model.addAttribute("logo", "yeah man");
        model.addAttribute("fruits", new String[]{"Apple", "Orange", "Strawberry", "Mango"});
        return new ModelAndView("template");
    }
}
